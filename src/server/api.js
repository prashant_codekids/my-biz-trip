const express = require('express');
var router = express.Router();
var request = require('request');

router.get('/getToken', (req, res, next) => {
    var options = {
        method: 'POST',
        url: 'https://apis.discover.com/auth/oauth/v2/token',
        qs: {
            grant_type: 'client_credentials',
            scope:
                'DCI_ATM COUNTRYACCEPTANCE CITYGUIDES DCILOUNGES_PROVIDER_LG DCILOUNGES_PROVIDER_DCIPL DCILOUNGES DCI_CURRENCYCONVERSION'
        },
        headers: {
            'cache-control': 'no-cache',
            'content-type': 'application/x-www-form-urlencoded',
            authorization:
                'Basic bDd4eGQ0ZmM1OTE3MjNlMzQ1NDVhODNmYmEzNmFhODY4ZTcyOjRiNTFhZGY4MWUwODQ3MDI4MTNjNWRlNjFkZDM3YzA2'
        }
    };

    request(options, function(error, response, body) {
        if (error) next(error);
        console.log(body);
        res.send(body);
    });
});

//to get lounges
router.get('/countryAcceptence/:countryCode/:token', (req, res, next) => {
    var url =
        'https://apis.discover.com/country-acceptance/v2/coverage?card_network=DCI&country_cca3=' +
        req.params.countryCode +
        '&sortby=name&sortdir=asc';
    request(
        {
            url: url,
            headers: {
                Accept: 'application/json',
                'x-dfs-api-plan': 'COUNTRYACCEPTANCE_SANDBOX',
                Authorization: 'Bearer ' + req.cookies.access_token
            },
            method: 'GET'
        },
        function(error, response, body) {
            if (error) next(error);
            console.log('Status', response.statusCode);
            console.log('Headers', JSON.stringify(response.headers));
            console.log('Reponse received', body);
            res.send(body);
        }
    );
});

module.exports = router;
