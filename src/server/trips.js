const db = require('./db.js');
const express = require('express');
var router = express.Router();

router.post('/add', async (req, res, next) => {
    let connection = await db.getConnection();
    let tripDetail = req.body.tripDetail;
    let query =
        'INSERT INTO TravelDestination(PhoneNo, FromDate, ToDate, Country, State, City, DailyAllowance, Street, Province)' +
        "VALUES ('" +
        tripDetail.phone +
        "','" +
        tripDetail.FromDate +
        "','" +
        tripDetail.ToDate +
        "','" +
        tripDetail.Country +
        "','" +
        tripDetail.State +
        "','" +
        tripDetail.City +
        "','" +
        tripDetail.DailyAllowance +
        "','" +
        tripDetail.Street +
        "','" +
        tripDetail.Province +
        "')";
    connection.query(query, (error, results, fields) => {
        connection.release();
        if (error) next(error);
        res.send(results);
    });
});
router.get('/:tripId', async (req, res, next) => {
    let connection = await db.getConnection();
    let query =
        "SELECT * FROM TravelDestination WHERE Tdid='" +
        req.params.tripId +
        "'";
    connection.query(query, (error, results, fields) => {
        connection.release();
        if (error) next(error);
        res.send(results[0]);
    });
});
router.delete('/delete/:tdid', async (req, res, next) => {
    let connection = await db.getConnection();
    let query =
        "DELETE FROM TravelDestination WHERE Tdid='" + req.params.tdid + "'";
    connection.query(query, (error, results, fields) => {
        connection.release();
        if (error) next(error);
        res.send(results);
    });
});
router.get('/', async (req, res, next) => {
    let connection = await db.getConnection();
    let query =
        "SELECT * FROM TravelDestination WHERE PhoneNo = '" +
        req.cookies.phone +
        "'";
    connection.query(query, (error, results, fields) => {
        connection.release();
        if (error) next(error);
        res.send(results);
    });
});

module.exports = router;
