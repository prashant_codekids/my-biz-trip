const mysql = require('mysql');
var pool;
const getConnection = async () => {
    if (!pool) {
        pool = mysql.createPool({
            connectionLimit: 4,
            host: 'remotemysql.com',
            user: 'oCfcM5o3Ou',
            password: 'QlgxKyEn2i',
            database: 'oCfcM5o3Ou'
        });
        console.log('=============Creating conection pool=================');
        pool.on('acquire', function(connection) {
            console.log('Connection %d acquired', connection.threadId);
        });
        pool.on('enqueue', function() {
            console.log('Waiting for available connection slot');
        });
        pool.on('release', function(connection) {
            console.log('Connection %d released', connection.threadId);
        });
    }
    let conPromise = new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) throw err;
            return resolve(connection);
        });
    });
    let connection = await conPromise;
    return connection;
};
module.exports = { getConnection };
