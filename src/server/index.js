module.exports = app => {
    const cookieParser = require('cookie-parser');
    const bodyParser = require('body-parser');
    const multer = require('multer'); // v1.0.5
    const upload = multer();
    const trips = require('./trips.js');
    const user = require('./user.js');
    const api = require('./api.js');
    app.use(cookieParser());
    app.use(bodyParser.json());
    app.use((req, res, next) => {
        console.log('params==>>', req.params);
        console.log('body==>', req.body);
        console.log('Cookie==>>', req.cookies);
        next();
    });

    app.use('/trips', trips);
    app.use('/user', user);
    app.use('/api', api);
    app.all('*', (re, res) => {
        res.status(404).send('not found');
    });
    app.use(function(err, req, res, next) {
        console.error(err.stack);
        res.status(500).send({ error: err });
    });
};
