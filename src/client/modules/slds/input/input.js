import LightningElementWithSLDS from '../../lib/lightningElementWithSLDS.js';
import { api, track } from 'lwc';

export default class Input extends LightningElementWithSLDS{
    @api name;
    @api required;
    @api disabled = false;
    @api label = '';
    @api value = null;
    @api labelHidden = false;
    @track isComponentValid = true;

    @track errorMessage;
    @api type = 'text';
    @api placeholder = '';
    @track inputId;
    connectedCallback(){
        this.inputId = new Date().getTime() + '';
    }
    onValueChange(e){
        this.value = e.target.value;
        if(this.checkValidity()){
            this.dispatchEvent(
                new CustomEvent('validinputvalue' , {detail: {value: this.value}})
            );
        }
    }
    @api
    get isValid(){
        return this.checkValidity();
    }
    checkValidity(){
        if(this.disabled){
            this.isComponentValid = true;
            this.errorMessage = ''
            return true;
        }
        let valid = true;
        if(this.required){
            if(!this.value){
                this.errorMessage = 'This field is required';
                valid = false;
            }
        }
        if(valid && this.value && this.type === 'email'){
            let reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(!reg.test(this.value)){
                this.errorMessage = 'Email Id is invalid';
                valid = false;
            }
        }
        if(valid && this.value && this.type === 'phone'){
            let reg = /^[0-9]{9,12}$/;
            if(!reg.test(this.value)){
                this.errorMessage = 'Phone Number is invalid';
                valid = false;
            }
        }
        if(valid){
            this.errorMessage = ''
        }
        this.isComponentValid = valid;
        return valid;
    }
    get inputClass(){
        return this.isComponentValid ? 'slds-form-element' : 'slds-form-element slds-has-error';
    }
}