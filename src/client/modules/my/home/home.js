import LightningElementWithSLDS from '../../lib/lightningElementWithSLDS.js';
import axios from 'axios';
import Cookies from 'js-cookie';
import { track } from 'lwc';

export default class Home extends LightningElementWithSLDS {
    @track trips = [];
    travelIcon =
        this.iconPath + '/utility-sprite/svg/symbols.svg#travel_and_places';
    addIcon = this.iconPath + '/utility-sprite/svg/symbols.svg#add';
    closeIcon = this.iconPath + '/utility-sprite/svg/symbols.svg#close';
    get isTripAvailable() {
        return this.trips && this.trips.length > 0;
    }
    connectedCallback() {
        axios
            .get('/trips')
            .then(res => {
                if (res.status === 200) {
                    console.log(res.data);
                    this.trips = res.data.map(item => {
                        return {
                            FromDate: item.FromDate
                                ? new Date(item.FromDate).toDateString()
                                : '',
                            ToDate: item.ToDate
                                ? new Date(item.FromDate).toDateString()
                                : '',
                            City: item.City,
                            State: item.State,
                            Country: item.Country,
                            Tdid: item.Tdid,
                            In: this.calculateDays(item.FromDate, item.ToDate)
                        };
                    });
                }
            })
            .catch(err => {
                console.log('error: ', err);
            });
    }
    addTrip() {
        this.dispatchEvent(
            new CustomEvent('navigate', {
                bubbles: true,
                composed: true,
                detail: { path: '/add-trip' }
            })
        );
    }
    calculateDays(from, to) {
        if (from && to) {
            let t1 = new Date(from).getTime();
            let t2 = new Date(to).getTime();
            let days = Math.round((t2 - t1) / (24 * 3600 * 1000));
            return days;
        }
        return '';
    }
    deleteTrip(e) {
        console.log(e.target.dataset.tdid);
        let tdid = e.target.dataset.tdid;
        axios
            .delete('/trips/delete/' + tdid)
            .then(res => {
                if (res.status === 200) {
                    let requiredIndex;
                    for (let i = 0; i < this.trips.length; i++) {
                        if (this.trips[i].Tdid == tdid) {
                            requiredIndex = tdid;
                            break;
                        }
                    }
                    this.trips = JSON.parse(
                        JSON.stringify(this.trips.splice(requiredIndex, 1))
                    );
                }
            })
            .catch(err => {
                console.log(err);
            });
    }
    viewDetails(e) {
        console.log(e.target.dataset.tdid);
        let tdid = e.target.dataset.tdid;
        this.dispatchEvent(
            new CustomEvent('navigate', {
                bubbles: true,
                composed: true,
                detail: { path: '/trip/' + tdid }
            })
        );
    }
}
