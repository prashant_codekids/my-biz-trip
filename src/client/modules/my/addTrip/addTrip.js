import LightningElementWithSLDS from '../../lib/lightningElementWithSLDS.js';
import Cookies from 'js-cookie';
import { getValue } from '../../lib/mylib.js';
import axios from 'axios';
import { track } from 'lwc';

export default class AddTrip extends LightningElementWithSLDS {
    travelIcon =
        this.iconPath + '/utility-sprite/svg/symbols.svg#travel_and_places';
    saveIcon = this.iconPath + '/utility-sprite/svg/symbols.svg#save';
    closeIcon = this.iconPath + '/utility-sprite/svg/symbols.svg#close';
    @track errorMsg = '';
    save() {
        let inputs = this.template.querySelectorAll('slds-input');
        let formData = {};
        formData.phone = Cookies.get('phone');
        if (inputs && inputs.length > 0) {
            let isAllValid = true;
            for (let i = 0; i < inputs.length; i++) {
                let isThisValid = inputs[i].isValid;
                if (!isThisValid) {
                    isAllValid = false;
                } else {
                    formData[inputs[i].name] = getValue(inputs[i].value);
                }
            }
            if (!isAllValid) {
                this.errorMsg = 'Please correct above errros!';
            } else {
                axios
                    .post('/trips/add', {
                        tripDetail: formData
                    })
                    .then(res => {
                        if (res.status === 200) {
                            this.errorMsg = '';
                            this.dispatchEvent(
                                new CustomEvent('navigate', {
                                    bubbles: true,
                                    composed: true,
                                    detail: { path: '/' }
                                })
                            );
                        } else {
                            this.errorMsg =
                                'Error in saving data. Please try again!';
                        }
                    })
                    .catch(error => {
                        // handle error
                        this.errorMsg =
                            'Error in saving data. Please try again!';
                    });
            }
        }
    }
    cancel() {
        this.dispatchEvent(
            new CustomEvent('navigate', {
                bubbles: true,
                composed: true,
                detail: { path: '/' }
            })
        );
    }
    get isErrror() {
        return this.errorMsg !== '';
    }
}
