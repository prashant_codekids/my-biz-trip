import LightningElementWithSLDS from '../../lib/lightningElementWithSLDS.js';
import Navigo from 'navigo';
import Cookies from 'js-cookie';
import { track, createElement } from 'lwc';
import { getValue } from '../../lib/mylib.js';
const axios = require('axios').default;

export default class App extends LightningElementWithSLDS {
    isRendered;
    @track isLogin = false;
    router = new Navigo(null, true);
    constructor() {
        super();
        this.router.on({
            '/': () => {
                if (this.isLoggedIn()) {
                    (async () => {
                        const { default: Home } = await import('my/home');
                        this.setPage('my-home', Home);
                    })();
                }
            },
            '/trip/:tripId': (data) => {
                 if (this.isLoggedIn()) {
                    (async () => {
                        const { default: TripDetail } = await import('my/tripDetail');
                        this.setPage('my-trip-detail', TripDetail, data);
                    })();
                 }
            },
            '/add-trip': () => {
                (async () => {
                    const { default: AddTrip } = await import('my/addTrip');
                    this.setPage('my-add-trip', AddTrip);
                })();
            },
            '/login': async () => {
                if (!Cookies.get('phone')) {
                    const { default: Login } = await import('my/login');
                    this.setPage('my-login', Login);
                } else {
                    this.router.navigate('/');
                }
            },
            '/signup': async () => {
                if (!Cookies.get('phone')) {
                    const { default: Signup } = await import('my/signup');
                    this.setPage('my-signup', Signup);
                } else {
                    this.router.navigate('/');
                }
            }
        });
        this.router.notFound(async () => {
            if (this.isLoggedIn()) {
                const { default: NotFound } = await import('my/notFound');
                this.setPage('my-not-found', NotFound);
            } else {
                this.router.navigate('/login');
            }
        });
    }
    connectedCallback(){
        this.getToken();
        //this.getLounges();
        
    }
    getToken(){
        let token = Cookies.get('access_token');
        let tokenTime = Cookies.get('token_time');
        let tokenOldMinutes;
        if(token && token !== 'undefined' && tokenTime){
            tokenOldMinutes = (new Date().getTime() - new Date(Number(tokenTime)).getTime())/(1000 * 60);
        }
        if(!tokenOldMinutes || tokenOldMinutes > 55){
            axios.get('/api/getToken').then(res => {
                if(res.status === 200){
                    console.log(res);
                    Cookies.set('access_token', res.data.access_token);
                    Cookies.set('token_time', new Date().getTime());
                }
            });
        }
    }
    renderedCallback() {
        if (!this.isRendered) {
            this.router.resolve();
            this.isRendered = true;
        }
        this.router.updatePageLinks();
    }
    //dynamically laoding components
    setPage(tag, component, data = {}) {
        const el = createElement(tag, {
            is: component,
            fallback: false
        });
        Object.assign(el, data);
        let container = this.template.querySelector('.container');
        while (container.firstChild) {
            container.removeChild(container.firstChild);
        }
        container.appendChild(el);
    }
    //handle navigation events from child
    handleNavigation(e) {
        console.log('navigate event details: ', e);
        if (e.detail) {
            this.router.navigate(e.detail.path);
        }
    }
    isLoggedIn() {
        let phoneNumber = Cookies.get('phone');
        if (phoneNumber) {
            this.isLogin = true;
            return true;
        }
        //go to login/signup page
        this.router.navigate('/login');
        return false;
    }

    logOut() {
        Cookies.remove('phone');
        Cookies.remove('userData');
        this.collapseMenu();
        this.isLogin = false;
        this.router.navigate('/login');

    }
    collapseMenu(){
        this.template.querySelector('.menu-btn').checked = false;
    }
}
