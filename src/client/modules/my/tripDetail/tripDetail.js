import LightningElementWithSLDS from '../../lib/lightningElementWithSLDS.js';
import Cookies from 'js-cookie';
import { getValue } from '../../lib/mylib.js';
import axios from 'axios';
import { track, api } from 'lwc';

export default class TripDetail extends LightningElementWithSLDS {
    @api tripId;
    travelIcon =
        this.iconPath + '/utility-sprite/svg/symbols.svg#travel_and_places';
    saveIcon = this.iconPath + '/utility-sprite/svg/symbols.svg#save';
    closeIcon = this.iconPath + '/utility-sprite/svg/symbols.svg#close';
    @track errorMsg = '';
    @track tripDetail;
    @track countryDetails = {};
    @track coverageData;
    @track coverageClass;
    connectedCallback() {
        console.log('tripId: ', this.tripId);
        this.getTripDetails();
    }

    getTripDetails() {
        axios
            .get('/trips/' + this.tripId)
            .then(res => {
                if (res.status == 200) {
                    this.tripDetail = res.data;
                    this.countryAcceptence();
                }
            })
            .catch(err => {
                console.log(err);
            });
    }
    countryAcceptence() {
        let countryMap = {
            Australia: 'AUS',
            India: 'IND',
            USA: 'USA',
            UK: 'UK',
            Afghanistan: 'AFG'
        };
        let countryCode = countryMap[this.tripDetail.Country];
        let ref = this;
        axios
            .get('/api/countryAcceptence/' + countryCode)
            .then(function(res) {
                console.log(res);
                ref.countryDetails = res.data.result[0];
                ref.getCoverageValue(ref);
            })
            .catch(e => {
                console.log(e);
            });
    }
    get coverage() {
        //return this.getCoverageValue(this);
    }
    get isCountry() {
        if (this.countryDetails) {
            return true;
        }
        return false;
    }
    get isTripDetail() {
        if (this.tripDetail) {
            return true;
        }
        return false;
    }

    getCoverageValue(ref) {
        if (ref.countryDetails && ref.countryDetails.atm_level) {
            let coverageMap = {
                'NO COVERAGE': 'No Coverage',
                J: 'Low',
                G: 'Medium',
                H: 'High'
            };
            ref.coverageData = coverageMap[ref.countryDetails.atm_level];
            if (ref.countryDetails.atm_level === 'J') {
                this.coverageClass = 'red';
            }
            if (ref.countryDetails.atm_level === 'G') {
                this.coverageClass = 'yellow';
            }
            if (ref.countryDetails.atm_level === 'H') {
                this.coverageClass = 'green';
            }
        }
    }
    cancel() {
        this.dispatchEvent(
            new CustomEvent('navigate', {
                bubbles: true,
                composed: true,
                detail: { path: '/' }
            })
        );
    }
    get isErrror() {
        return this.errorMsg !== '';
    }
}
