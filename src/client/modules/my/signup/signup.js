import LightningElementWithSLDS from '../../lib/lightningElementWithSLDS.js';
import axios from 'axios';
import { track } from 'lwc';
import { getValue } from '../../lib/mylib.js';
import Cookies from 'js-cookie';
export default class Signup extends LightningElementWithSLDS {
    @track isValid = true;
    @track user = {};
    signupUser() {
        let inputs = this.template.querySelectorAll('.inputClass');
        if (inputs && inputs.length > 0) {
            for (let i = 0; i < inputs.length; i++) {
                this.user[inputs[i].getAttribute('data-id')] = inputs[i].value;
                if (!inputs[i].isValid) {
                    this.isValid = false;
                }
            }
        }
        //Checking if values are valid and all the 4 values are filled
        if (/*this.isValid &&*/ this.user) {
            this.insertData();
        }
    }
    insertData() {
        axios
            .post('/user/add-user', {
                user: getValue(this.user)
            })
            .then(res => {
                console.log(res);
                if (res.status === 200) {
                    if (res.data.isSuccess) {
                        this.errorMsg = '';
                        Cookies.set('userData', res.data.result);
                        Cookies.set('phone', getValue(this.user.PhoneNo));
                        this.dispatchEvent(
                            new CustomEvent('navigate', {
                                bubbles: true,
                                composed: true,
                                detail: { path: '/' }
                            })
                        );
                    } else {
                        this.errorMsg =
                            'User not found! Please enter valid credentials';
                    }
                } else {
                    this.errorMsg = 'Error in log in. Please try again';
                }
            });
    }
    //Getters
    get userRoleIcon() {
        return this.iconPath + '/standard-sprite/svg/symbols.svg#user_role';
    }
}
