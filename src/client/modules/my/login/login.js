import LightningElementWithSLDS from '../../lib/lightningElementWithSLDS.js';
import Cookies from 'js-cookie';
import { getValue } from '../../lib/mylib.js';
import axios from 'axios';
import { track } from 'lwc';

export default class Login extends LightningElementWithSLDS {
    @track errorMsg = '';
    get loginIcon() {
        return this.iconPath + '/standard-sprite/svg/symbols.svg#user';
    }
    get buttonIcon() {
        return this.iconPath + '/utility-sprite/svg/symbols.svg#send';
    }
    login() {
        console.log('trying to login');
        let loginInput = this.template.querySelector('.login-input');
        console.log('input validity: ', loginInput.isValid);
        if (loginInput.isValid) {
            axios
                .post('/user/login', {
                    phone: getValue(loginInput.value)
                })
                .then(res => {
                    console.log(res);
                    if (res.status === 200) {
                        if (res.data.isSuccess) {
                            this.errorMsg = '';
                            Cookies.set('userData', res.data.result);
                            Cookies.set('phone', loginInput.value);
                            console.log(
                                'from cookie=>',
                                Cookies.get('userData')
                            );
                            this.dispatchEvent(
                                new CustomEvent('navigate', {
                                    bubbles: true,
                                    composed: true,
                                    detail: { path: '/' }
                                })
                            );
                        } else {
                            this.errorMsg =
                                'User not found! Please enter valid credentials';
                        }
                    } else {
                        this.errorMsg = 'Error in log in. Please try again';
                    }
                });
        }
    }
    get isError() {
        return this.errorMsg !== '';
    }
}
