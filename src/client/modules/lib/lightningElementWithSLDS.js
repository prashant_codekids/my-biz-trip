import {LightningElement, track} from 'lwc';

export default class LightningElementWithSLDS extends LightningElement {
    iconPath = '../../resources/slds/assets/icons';
    constructor() {
      super();
      const path =
        '../../resources/slds/assets/styles/salesforce-lightning-design-system.min.css';
      const styles = document.createElement('link');
      styles.href = path;
      styles.rel = 'stylesheet';
      this.template.appendChild(styles);
      const stylesFont = document.createElement('link');
      stylesFont.href = '../../resources/fonts/fonts.css';
      stylesFont.rel = 'stylesheet';
      this.template.appendChild(stylesFont);
   }
     
}